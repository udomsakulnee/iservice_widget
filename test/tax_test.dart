import 'package:flutter_test/flutter_test.dart';
import 'package:iservice_widget/tax.dart';

void main() {
  group('Tax', () {
    test('netIncome should start at 0', () {
      final tax = Tax();
      expect(tax.getNetIncome(), 0);
    });

    test('calculate netIncome 215,000', () {
      final tax = Tax();

      tax.setIncome(250000);
      tax.setTaxBreak(35000);
      tax.calculate();

      expect(tax.getNetIncome(), 215000);
    });

    test('calculate tax 3,250', () {
      final tax = Tax();

      tax.setIncome(250000);
      tax.setTaxBreak(35000);
      tax.calculate();

      expect(tax.getTax(), 3250);
    });

    test('calculate netIncome 430,000', () {
      final tax = Tax();

      tax.setIncome(500000);
      tax.setTaxBreak(70000);
      tax.calculate();

      expect(tax.getNetIncome(), 430000);
    });

    test('calculate tax 20,500', () {
      final tax = Tax();

      tax.setIncome(500000);
      tax.setTaxBreak(70000);
      tax.calculate();

      expect(tax.getTax(), 20500);
    });

    test('calculate netIncome 6,000,000', () {
      final tax = Tax();

      tax.setIncome(7000000);
      tax.setTaxBreak(1000000);
      tax.calculate();

      expect(tax.getNetIncome(), 6000000);
    });

    test('calculate tax 1,615,000', () {
      final tax = Tax();

      tax.setIncome(7000000);
      tax.setTaxBreak(1000000);
      tax.calculate();

      expect(tax.getTax(), 1615000);
    });
  });
}
