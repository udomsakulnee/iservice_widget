import 'package:flutter/material.dart';
import 'package:iservice_widget/widget/appbar.dart';
import 'package:iservice_widget/widget/package_box.dart';

class PageOne extends StatefulWidget {
  @override
  _PageOneState createState() => _PageOneState();
}

class _PageOneState extends State<PageOne> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(
        title: 'Widget 1',
      ),
      body: Container(
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              PackageBox(
                data: 3,
                price: 79,
                day: 10,
              ),
              PackageBox(
                data: 5,
                price: 89,
                day: 10,
              ),
              PackageBox(
                data: 10,
                price: 109,
                day: 10,
              ),
              PackageBox(
                data: 30,
                price: 199,
                day: 10,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
