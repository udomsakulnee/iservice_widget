import 'package:flutter/material.dart';
import 'package:iservice_widget/widget/appbar.dart';

class PageTen extends StatefulWidget {
  @override
  _PageTenState createState() => _PageTenState();
}

class _PageTenState extends State<PageTen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(
        title: 'Widget 10',
      ),
      body: Container()
    );
  }
}