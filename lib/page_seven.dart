import 'package:flutter/material.dart';
import 'package:iservice_widget/widget/appbar.dart';

class PageSeven extends StatefulWidget {
  @override
  _PageSevenState createState() => _PageSevenState();
}

class _PageSevenState extends State<PageSeven> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(
        title: 'Widget 7',
      ),
      body: Container()
    );
  }
}