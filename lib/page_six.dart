import 'package:flutter/material.dart';
import 'package:iservice_widget/widget/appbar.dart';

class PageSix extends StatefulWidget {
  @override
  _PageSixState createState() => _PageSixState();
}

class _PageSixState extends State<PageSix> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(
        title: 'Widget 6',
      ),
      body: Container()
    );
  }
}