import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class Announcement extends StatefulWidget {
  Announcement({
    Key key,
    @required this.imgList
  }) : super(key: key);

  final List<String> imgList;

  @override
  _AnnouncementState createState() => _AnnouncementState();
}

class _AnnouncementState extends State<Announcement> {
  int _current = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          CarouselSlider(
            options: CarouselOptions(
              autoPlay: true,
              viewportFraction: 1,
              aspectRatio: 750 / 388,
              onPageChanged: (index, reason) {
                setState(() {
                  _current = index;
                });
              },
            ),
            items: widget.imgList.map((item) => Image.asset(item)).toList(),
          ),
          Positioned(
            bottom: 0,
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: widget.imgList.map((url) {
                  int index = widget.imgList.indexOf(url);
                  return GestureDetector(
                      onTap: () {
                        setState(() {
                          _current = index;
                        });
                      },
                      child: Container(
                        width: 8,
                        height: 8,
                        margin:
                            EdgeInsets.symmetric(vertical: 10, horizontal: 3),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: _current == index
                              ? Colors.grey[800]
                              : Colors.grey[200],
                        ),
                      ));
                }).toList(),
              ),
            ),
          )
        ],
      ),
    );
  }
}
