import 'package:flutter/material.dart';

class TabContent {
  TabContent({
    this.key,
    @required this.title,
    @required this.child,
  });

  final Key key;
  final String title;
  final Widget child;
}

class TabList extends StatefulWidget {
  TabList({Key key, @required this.children}) : super(key: key);

  final List<TabContent> children;

  @override
  _TabListState createState() => _TabListState();
}

class _TabListState extends State<TabList> {
  int _current = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            child: Row(
              children: widget.children.asMap().entries.map(
                (entry) {
                  return Expanded(
                    child: GestureDetector(
                      onTap: () => setState(() {
                        _current = entry.key;
                      }),
                      child: Container(
                        margin: EdgeInsets.only(
                          left: entry.key == 0 ? 0 : 3,
                          right:
                              entry.key == widget.children.length - 1 ? 0 : 3,
                        ),
                        padding: EdgeInsets.only(
                          top: 12,
                          bottom: 12,
                        ),
                        decoration: BoxDecoration(
                          color: _current == entry.key
                              ? Colors.pink
                              : Colors.grey[350],
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10),
                          ),
                        ),
                        child: Text(
                          entry.value.title,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: _current == entry.key
                                ? Colors.white
                                : Colors.grey[900],
                          ),
                        ),
                      ),
                    ),
                  );
                },
              ).toList(),
            ),
          ),
          Divider(
            height: 2,
            thickness: 2,
            color: Colors.pink,
          ),
          Stack(
            overflow: Overflow.visible,
            children: [
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(
                    color: Colors.grey[350],
                  ),
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10),
                    bottomRight: Radius.circular(10),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey[300],
                      spreadRadius: 2,
                      blurRadius: 5,
                      offset: Offset(0, 3),
                    ),
                  ],
                ),
                width: MediaQuery.of(context).size.width,
                child: widget.children[_current].child,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: widget.children.asMap().entries.map((entry) {
                  return Container(
                    transform: Matrix4.translationValues(10, -16, 0)
                      ..rotateZ(0.775),
                    color: _current == entry.key ? Colors.pink : null,
                    width: 20,
                    height: 20,
                  );
                }).toList(),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
