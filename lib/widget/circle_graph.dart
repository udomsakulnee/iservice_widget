import 'package:flutter/material.dart';

enum CircleGraphType { voice, data }

class CircleGraph extends StatelessWidget {
  CircleGraph({
    Key key,
    this.type = CircleGraphType.voice,
    this.used = 0,
    this.total = 0,
    this.size = 160,
  }) : super(key: key);

  final CircleGraphType type;
  final int used;
  final int total;
  final double size;

  @override
  Widget build(BuildContext context) {
    final double percent =
        this.total > 0 && this.type == CircleGraphType.voice
            ? this.used / this.total
            : 0.0;
    return RotatedBox(
      quarterTurns: 3,
      child: Container(
        width: this.size,
        height: this.size,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(this.size / 2),
          gradient: SweepGradient(
            colors: [
              Colors.orange[300],
              Colors.grey[300],
            ],
            stops: [percent, percent],
          ),
        ),
        child: RotatedBox(
          quarterTurns: 1,
          child: FractionallySizedBox(
            widthFactor: 0.9,
            heightFactor: 0.9,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(this.size / 2),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    this.type == CircleGraphType.voice ? 'Voice' : 'Data',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text('Main + Extra'),
                  Container(
                    margin: EdgeInsets.only(
                      top: 8,
                      bottom: 8,
                    ),
                    child: Text(
                      this.type == CircleGraphType.voice
                          ? this.used.toString()
                          : 'Unlimited',
                      style: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                        color: Colors.orange[300],
                      ),
                    ),
                  ),
                  this.type == CircleGraphType.voice
                      ? Text('of ' + this.total.toString() + ' min')
                      : Text(''),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}