import 'package:flutter/material.dart';

PreferredSizeWidget buildAppBar({String title}) {
  return AppBar(
    backgroundColor: Colors.transparent,
    title: Text(title),
    brightness: Brightness.light,
    elevation: 4,
    centerTitle: true,
    shadowColor: Colors.grey[300],
    flexibleSpace: Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: <Color>[Colors.blueGrey[50], Colors.blueGrey[200]],
        ),
      ),
    ),
  );
}
