import 'package:flutter/material.dart';

class InboxContent {
  InboxContent(this.title, {this.icon, this.notify = 0});

  @required
  final String title;
  final IconData icon;
  final int notify;
}

class InboxList extends StatelessWidget {
  InboxList({this.children});

  final List<InboxContent> children;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        children: [
          Card(
            elevation: 2,
            child: Column(
              children: _buildContent(context, this.children),
            ),
          ),
        ],
      ),
    );
  }

  List<Widget> _buildContent(BuildContext context, List<InboxContent> contents) {
    return contents.map(
      (content) {
        return GestureDetector(
          onTap: () => Navigator.pushNamed(context, '/page_five'),
          child: Container(
            padding: EdgeInsets.all(15),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  color: Colors.grey[200],
                ),
              ),
            ),
            child: Row(
              children: [
                Container(
                    child: Icon(content.icon),
                    margin: EdgeInsets.only(
                      right: 12,
                    )),
                Expanded(
                  child: Text(
                    content.title,
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  ),
                ),
                Container(
                  width: 24,
                  height: 24,
                  decoration: BoxDecoration(
                    color: content.notify > 0 ? Colors.pink : null,
                    borderRadius: BorderRadius.circular(12),
                  ),
                  child: Center(
                    child: content.notify > 0
                        ? Text(
                            content.notify.toString(),
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          )
                        : Text(''),
                  ),
                ),
                Container(
                  child: Icon(
                    Icons.chevron_right,
                    size: 26,
                    color: Colors.pink,
                  ),
                ),
              ],
            ),
          ),
        );
      },
    ).toList();
  }
}
