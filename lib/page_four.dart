import 'package:flutter/material.dart';
import 'package:iservice_widget/widget/appbar.dart';
import 'package:iservice_widget/widget/inbox_list.dart';

class PageFour extends StatefulWidget {
  @override
  _PageFourState createState() => _PageFourState();
}

class _PageFourState extends State<PageFour> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(
        title: 'Widget 4',
      ),
      body: Container(
        child: InboxList(
          children: [
            InboxContent(
              'Bills & Payment',
              icon: Icons.payment,
              notify: 32,
            ),
            InboxContent(
              'Data/Voice Balance',
              icon: Icons.data_usage,
              notify: 30,
            ),
            InboxContent(
              'System Maintenance',
              icon: Icons.build,
            ),
            InboxContent(
              'Promotions',
              icon: Icons.celebration,
              notify: 3,
            ),
            InboxContent(
              'News & Updates',
              icon: Icons.system_update,
              notify: 7,
            ),
          ],
        ),
      ),
    );
  }
}
