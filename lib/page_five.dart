import 'package:flutter/material.dart';
import 'package:iservice_widget/widget/appbar.dart';
import 'package:iservice_widget/widget/circle_graph.dart';
import 'package:iservice_widget/widget/tab_list.dart';

class PageFive extends StatefulWidget {
  @override
  _PageFiveState createState() => _PageFiveState();
}

class _PageFiveState extends State<PageFive> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(
        title: 'Widget 5',
      ),
      body: Container(
        padding: EdgeInsets.all(12),
        child: TabList(
          children: [
            TabContent(
              title: 'Current Usage',
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.all(16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        CircleGraph(
                          used: 30,
                          total: 150,
                        ),
                        CircleGraph(
                          type: CircleGraphType.data,
                        ),
                      ],
                    ),
                  ),
                  Divider(
                    height: 1,
                    thickness: 1,
                    color: Colors.grey[300],
                  ),
                  Container(
                    padding: EdgeInsets.all(12),
                    child: Row(
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                            right: 4,
                          ),
                          child: Icon(
                            Icons.error,
                            color: Colors.pink,
                          ),
                        ),
                        Expanded(
                          child: Text(
                            'Out of Package',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Text(
                          '฿',
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.pink,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(
                          '0',
                          style: TextStyle(
                            color: Colors.pink,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(
                          '.00',
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.pink,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Container(
                          child: Icon(
                            Icons.chevron_right,
                            size: 26,
                            color: Colors.pink,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            TabContent(
              title: 'Bill Details',
              child: Container(
                padding: EdgeInsets.all(16),
                child: Text('-- no data --'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
