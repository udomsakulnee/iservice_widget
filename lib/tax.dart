class Tax {
  double _income = 0.0;
  double _taxBreak = 0.0;
  double _netIncome = 0.0;
  double _taxTotal = 0.0;

  void setIncome(double income) {
    _income = income;
  }

  void setTaxBreak(double taxBreak) {
    _taxBreak = taxBreak;
  }

  void calculate() {
    _netIncome = _income - _taxBreak;
    double income = _netIncome;
    double tax = 0.0;

    // 0 - 150,000 (0 %)
    if (income < 150000) {
      income = 0;
    }
    else {
      income -= 150000;
    }

    // 150,001 - 300,000 (5 %)
    if (income > 0) {
      if (income < 150000) {
        tax += (income * 0.05);
        income = 0;
      }
      else {
        tax += (150000 * 0.05);
        income -= 150000;
      }
    }

    // 300,001 - 500,000 (10 %)
    if (income > 0) {
      if (income < 200000) {
        tax += (income * 0.1);
        income = 0;
      }
      else {
        tax = tax + (200000 * 0.1);
        income -= 200000;
      }
    }

    // 500,001 - 750,000 (15 %)
    if (income > 0) {
      if (income < 250000) {
        tax = tax + (income * 0.15);
        income = 0;
      }
      else {
        tax = tax + (250000 * 0.15);
        income -= 250000;
      }
    }

    // 750,001 - 1,000,000 (20 %)
    if (income > 0) {
      if (income < 250000) {
        tax = tax + (income * 0.2);
        income = 0;
      }
      else {
        tax = tax + (250000 * 0.2);
        income -= 250000;
      }
    }

    // 1,000,001 - 2,000,000 (25 %)
    if (income > 0) {
      if (income < 1000000) {
        tax = tax + (income * 0.25);
        income = 0;
      }
      else {
        tax = tax + (1000000 * 0.25);
        income -= 1000000;
      }
    }

    // 2,000,001 - 5,000,000 (30 %)
    if (income > 0) {
      if (income < 3000000) {
        tax = tax + (income * 0.3);
        income = 0;
      }
      else {
        tax = tax + (3000000 * 0.3);
        income -= 3000000;
      }
    }

    // 5,000,001 - infinite (35 %)
    if (income > 0) {
      tax = tax + (income * 0.35);
      income = 0;
    }

    _taxTotal = tax;
  }

  double getNetIncome() {
    return _netIncome;
  }

  double getTax() {
    return _taxTotal;
  }
}