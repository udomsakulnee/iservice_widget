import 'package:flutter/material.dart';
import 'package:iservice_widget/widget/appbar.dart';

class PageNine extends StatefulWidget {
  @override
  _PageNineState createState() => _PageNineState();
}

class _PageNineState extends State<PageNine> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(
        title: 'Widget 9',
      ),
      body: Container()
    );
  }
}