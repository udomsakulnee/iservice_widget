import 'package:flutter/material.dart';
import 'package:iservice_widget/widget/appbar.dart';

class PageEight extends StatefulWidget {
  @override
  _PageEightState createState() => _PageEightState();
}

class _PageEightState extends State<PageEight> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(
        title: 'Widget 8',
      ),
      body: Container()
    );
  }
}