import 'package:flutter/material.dart';
import 'package:iservice_widget/widget/announcement.dart';
import 'package:iservice_widget/widget/appbar.dart';

final List<String> imgList = [
  'assets/5gmax.png',
  'assets/gomax.jpg',
  'assets/pay_for_other.png',
  'assets/tol_banner.png',
  'assets/true_point.jpg',
];

class PageThree extends StatefulWidget {
  @override
  _PageThreeState createState() => _PageThreeState();
}

class _PageThreeState extends State<PageThree> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(
        title: 'Widget 3',
      ),
      body: Container(
        child: Announcement(
          imgList: imgList,
        ),
      ),
    );
  }
}