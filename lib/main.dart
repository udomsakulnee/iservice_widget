import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:iservice_widget/page_eight.dart';
import 'package:iservice_widget/page_five.dart';
import 'package:iservice_widget/page_four.dart';
import 'package:iservice_widget/page_nine.dart';
import 'package:iservice_widget/page_one.dart';
import 'package:iservice_widget/page_seven.dart';
import 'package:iservice_widget/page_six.dart';
import 'package:iservice_widget/page_ten.dart';
import 'package:iservice_widget/page_two.dart';
import 'package:iservice_widget/page_three.dart';
import 'package:iservice_widget/widget/appbar.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Iservice Widget',
      theme: ThemeData(
        primaryColor: Colors.blueGrey[200],
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => MyHomePage(title: 'Iservice Widget'),
        '/page_one': (context) => PageOne(),
        '/page_two': (context) => PageTwo(),
        '/page_three': (context) => PageThree(),
        '/page_four': (context) => PageFour(),
        '/page_five': (context) => PageFive(),
        '/page_six': (context) => PageSix(),
        '/page_seven': (context) => PageSeven(),
        '/page_eight': (context) => PageEight(),
        '/page_nine': (context) => PageNine(),
        '/page_ten': (context) => PageTen(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  static const platform = const MethodChannel('com.example.test/data');
  
  Future<void> _receiveFromHost(MethodCall call) async {
    try {
      if (call.method == "nativeToFlutter") {
        final String data = call.arguments;
        print(data);
      }
    } on PlatformException catch (error) {
      print(error);
    }
  }

  _MyHomePageState() {
    platform.setMethodCallHandler(_receiveFromHost);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(
        title: widget.title,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 20, bottom: 15),
              child: Text(
                'Widgets',
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                RaisedButton(
                    child: Text('Widget 1'),
                    onPressed: () {
                      Navigator.pushNamed(context, '/page_one');
                    }),
                RaisedButton(
                    child: Text('Widget 2'),
                    onPressed: () {
                      Navigator.pushNamed(context, '/page_two');
                    })
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                RaisedButton(
                    child: Text('Widget 3'),
                    onPressed: () {
                      Navigator.pushNamed(context, '/page_three');
                    }),
                RaisedButton(
                    child: Text('Widget 4'),
                    onPressed: () {
                      Navigator.pushNamed(context, '/page_four');
                    })
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                RaisedButton(
                    child: Text('Widget 5'),
                    onPressed: () {
                      Navigator.pushNamed(context, '/page_five');
                    }),
                RaisedButton(
                    child: Text('Widget 6'),
                    onPressed: () {
                      Navigator.pushNamed(context, '/page_six');
                    })
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                RaisedButton(
                    child: Text('Widget 7'),
                    onPressed: () {
                      Navigator.pushNamed(context, '/page_seven');
                    }),
                RaisedButton(
                    child: Text('Widget 8'),
                    onPressed: () {
                      Navigator.pushNamed(context, '/page_eight');
                    })
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                RaisedButton(
                    child: Text('Widget 9'),
                    onPressed: () {
                      Navigator.pushNamed(context, '/page_nine');
                    }),
                RaisedButton(
                    child: Text('Widget 10'),
                    onPressed: () {
                      Navigator.pushNamed(context, '/page_ten');
                    })
              ],
            ),
            RaisedButton(
                child: Text('Close Fultter'),
                onPressed: () {
                  SystemNavigator.pop(animated: true);
                })
          ],
        ),
      ),
    );
  }
}
