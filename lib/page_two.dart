import 'package:flutter/material.dart';
import 'package:iservice_widget/widget/appbar.dart';
import 'package:iservice_widget/widget/circle_graph.dart';

class PageTwo extends StatefulWidget {
  @override
  _PageTwoState createState() => _PageTwoState();
}

class _PageTwoState extends State<PageTwo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(
        title: 'Widget 2',
      ),
      body: Container(
        child: Row(
          children: [
            CircleGraph(
              used: 30,
              total: 150,
            ),
            CircleGraph(
              type: CircleGraphType.data,
            ),
          ],
        ),
      ),
    );
  }
}
